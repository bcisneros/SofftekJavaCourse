package com.softtek.courses.java;

import com.softtek.courses.java.util.printer.Printer;
import com.softtek.courses.java.util.reader.Reader;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Academia
 */
public class MainTest {

    public MainTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Main.
     */
    @Test
    public void testMain() {
        Printer mockPrinter = EasyMock.createMock(Printer.class);
        Reader mockReader = EasyMock.createMock(Reader.class);
        mockPrinter.print("Welcome to softtek!");
        EasyMock.expectLastCall().times(1);
        mockPrinter.print("Introduce palindrome:\n");
        EasyMock.expectLastCall().times(1);
        EasyMock.expect(mockReader.read()).andReturn("oso");
        mockPrinter.print("oso has Low complexity.");
        EasyMock.expectLastCall().times(1);
        EasyMock.replay(mockPrinter);
        EasyMock.replay(mockReader);
        Main.printer = mockPrinter;
        Main.reader = mockReader;
        Main.main(new String[]{});
        EasyMock.verify(mockPrinter);
        EasyMock.verify(mockReader);
    }

    @Test
    public void testAnotherPalindrome() {
        Printer mockPrinter = EasyMock.createMock(Printer.class);
        Reader mockReader = EasyMock.createMock(Reader.class);
        mockPrinter.print("Welcome to softtek!");
        EasyMock.expectLastCall().times(1);
        mockPrinter.print("Introduce palindrome:\n");
        EasyMock.expectLastCall().times(1);
        EasyMock.expect(mockReader.read()).andReturn("this is not a palindrome");
        mockPrinter.print("this is not a palindrome is not a palindrome");
        EasyMock.expectLastCall().times(1);
        EasyMock.replay(mockPrinter);
        EasyMock.replay(mockReader);
        Main.printer = mockPrinter;
        Main.reader = mockReader;
        Main.main(new String[]{});
        EasyMock.verify(mockPrinter);
        EasyMock.verify(mockReader);
    }
}
