package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Benjamin
 */
public class BenjaminPalindromeAnalyzerTest extends PalindromeAnalyzerTest {

    @Override
    protected PalindromeAnalyzer getAnalyzerImplementation() {
        return new BenjaminPalindromeAnalyzer();
    }

}
