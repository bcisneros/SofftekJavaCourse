package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class GeraBooleanStringParserTest extends BooleanStringParserTest {

    /**
     *
     * @return
     */
    @Override
    protected BooleanStringParser getParserImplementation() {
        return new GeraBooleanStringParser();
    }

}
