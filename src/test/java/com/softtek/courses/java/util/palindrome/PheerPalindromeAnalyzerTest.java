package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class PheerPalindromeAnalyzerTest extends PalindromeAnalyzerTest {

    @Override
    protected PalindromeAnalyzer getAnalyzerImplementation() {
        return new PheerPalindromeAnalyzer();
    }

}
