package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class GeraPalindromeAnalyzerTest extends PalindromeAnalyzerTest {

    @Override
    protected PalindromeAnalyzer getAnalyzerImplementation() {
        return new GeraPalindromeAnalyzer();
    }

}
