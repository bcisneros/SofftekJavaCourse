package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class MarinBooleanStringParserTest extends BooleanStringParserTest {

    /**
     *
     * @return
     */
    @Override
    protected BooleanStringParser getParserImplementation() {
        return new MarinBooleanStringParser();
    }

}
