package com.softtek.courses.java.util.parser;

import java.util.ArrayList;
import java.util.List;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Benjamin
 */
@RunWith(JUnitParamsRunner.class)
public abstract class BooleanStringParserTest {

    private final BooleanStringParser parser;

    @SuppressWarnings("unchecked")
    public BooleanStringParserTest() {
        parser = getParserImplementation();
    }

    /**
     * Test of parseString method, of class BooleanStringParser.
     *
     * @param value
     */
    @Test
    @Parameters(method = "getStringTrueValues")
    public void testParseStringTrueValues(String value) {
        assertTrue(parser.parseString(value));
    }

    /**
     *
     * @param value
     */
    @Test
    @Parameters(method = "getStringFalseValues")
    public void testParseStringFalseValues(String value) {
        assertFalse(parser.parseString(value));
    }

    /**
     *
     * @param value
     */
    @Test
    @Parameters(method = "getStringNullValues")
    public void testParseStringNullValues(String value) {
        assertNull(parser.parseString(value));
    }

    /**
     *
     * @return
     */
    public static List<String> getStringFalseValues() {
        List<String> trueStringValues = new ArrayList<>();
        trueStringValues.add("no");
        trueStringValues.add("NO");
        trueStringValues.add("No");
        trueStringValues.add("nO");
        trueStringValues.add("0");
        trueStringValues.add("-1");
        trueStringValues.add("false");
        trueStringValues.add("FALSE");
        return trueStringValues;
    }

    /**
     *
     * @return
     */
    public static List<String> getStringTrueValues() {
        List<String> trueStringValues = new ArrayList<>();
        trueStringValues.add("si");
        trueStringValues.add("SI");
        trueStringValues.add("Si");
        trueStringValues.add("sI");
        trueStringValues.add("yes");
        trueStringValues.add("YES");
        trueStringValues.add("Yes");
        trueStringValues.add("YEs");
        trueStringValues.add("1");
        trueStringValues.add("true");
        trueStringValues.add("TRUE");
        return trueStringValues;
    }

    /**
     *
     * @return
     */
    public static List<String> getStringNullValues() {
        List<String> trueStringValues = new ArrayList<>();
        trueStringValues.add(null);
        trueStringValues.add("whatever");
        trueStringValues.add("falses");
        return trueStringValues;
    }

    /**
     * Returns a parser implementation used to test
     *
     * @return The parser implementation
     */
    protected abstract BooleanStringParser getParserImplementation();
}
