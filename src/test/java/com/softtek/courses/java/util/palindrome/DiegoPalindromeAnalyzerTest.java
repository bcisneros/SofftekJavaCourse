package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class DiegoPalindromeAnalyzerTest extends PalindromeAnalyzerTest {

    @Override
    protected PalindromeAnalyzer getAnalyzerImplementation() {
        return new DiegoPalindromeAnalyzer();
    }

}
