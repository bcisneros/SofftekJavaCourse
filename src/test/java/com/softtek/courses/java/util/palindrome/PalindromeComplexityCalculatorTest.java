package com.softtek.courses.java.util.palindrome;

import org.easymock.EasyMock;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Academia
 */
public class PalindromeComplexityCalculatorTest {

    private final PalindromeComplexityCalculator calculator;

    public PalindromeComplexityCalculatorTest() {
        PalindromeAnalyzer mockAnalyzer = EasyMock.createMock(PalindromeAnalyzer.class);
        EasyMock.expect(mockAnalyzer.isPalindrome("oso")).andReturn(Boolean.TRUE);
        EasyMock.expect(mockAnalyzer.isPalindrome("Anita lava la tina")).andReturn(Boolean.TRUE);
        EasyMock.expect(mockAnalyzer.isPalindrome("Was it a car or a cat I saw?")).andReturn(Boolean.TRUE);

        EasyMock.expect(mockAnalyzer.isPalindrome("not a plaindrome string")).andReturn(Boolean.FALSE);
        EasyMock.expect(mockAnalyzer.isPalindrome(null)).andReturn(Boolean.FALSE);
        EasyMock.expect(mockAnalyzer.isPalindrome("%$&((%$%")).andReturn(Boolean.FALSE);
        EasyMock.expect(mockAnalyzer.isPalindrome("        ")).andReturn(Boolean.FALSE);

        EasyMock.replay(mockAnalyzer);
        calculator = new PalindromeComplexityCalculator(mockAnalyzer);
    }

    @Test
    public void testCalculateComplexity() {
        final String lowComplexityPalindrome = "oso";
        final String mediumComplexityPalindrome = "Anita lava la tina";
        final String highComplexityPalindrome = "Was it a car or a cat I saw?";
        try {
            assertSame(PalindromeComplexity.LOW, calculator.calculateComplexity(lowComplexityPalindrome));
            assertSame(PalindromeComplexity.MEDIUM, calculator.calculateComplexity(mediumComplexityPalindrome));
            assertSame(PalindromeComplexity.HIGH, calculator.calculateComplexity(highComplexityPalindrome));

        } catch (NotValidPalindromeException ex) {
            fail("Not exceptions is expected: " + ex);
        }

    }

    @Test
    public void testCalculateComplexityWithNonPalindrome() {
        try {
            calculator.calculateComplexity("not a plaindrome string");
            fail("Exception is expected");
        } catch (NotValidPalindromeException ex) {

        }
    }

    @Test
    public void testCalculateComplexityWithNullPalindrome() {
        try {
            calculator.calculateComplexity(null);
            fail("Exception is expected");
        } catch (NotValidPalindromeException ex) {

        }
    }

    @Test
    public void testCalculateComplexityWithOnlySymbolsPalindrome() {
        try {
            calculator.calculateComplexity("%$&((%$%");
            fail("Exception is expected");
        } catch (NotValidPalindromeException ex) {

        }
    }

    @Test(expected = NotValidPalindromeException.class)
    public void testCalculateComplexityWithOnlySpacessPalindrome() throws NotValidPalindromeException {
        calculator.calculateComplexity("        ");
    }

}
