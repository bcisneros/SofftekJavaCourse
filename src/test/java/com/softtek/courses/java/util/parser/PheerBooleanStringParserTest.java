package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class PheerBooleanStringParserTest extends BooleanStringParserTest {

    /**
     *
     * @return
     */
    @Override
    protected BooleanStringParser getParserImplementation() {
        return new PheerBooleanStringParser();
    }
}
