package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class FritzBooleanStringParserTest extends BooleanStringParserTest {

    /**
     *
     * @return
     */
    @Override
    protected BooleanStringParser getParserImplementation() {
        return new FritzBooleanStringParser();
    }

}
