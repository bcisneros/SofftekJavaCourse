package com.softtek.courses.java.util.parser;

/**
 *
 * @author Benjamin
 */
public class BenjaminBooleanStringParserTest extends BooleanStringParserTest {

    @Override
    protected BooleanStringParser getParserImplementation() {
        return new BenjaminBooleanStringParser();
    }

}
