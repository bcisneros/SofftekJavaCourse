package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class DiegoBooleanStringParserTest extends BooleanStringParserTest {

    /**
     *
     * @return
     */
    @Override
    protected BooleanStringParser getParserImplementation() {
        return new DiegoBooleanStringParser();
    }

}
