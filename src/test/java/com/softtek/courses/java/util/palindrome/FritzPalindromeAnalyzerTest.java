package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class FritzPalindromeAnalyzerTest extends PalindromeAnalyzerTest {

    @Override
    protected PalindromeAnalyzer getAnalyzerImplementation() {
        return new FritzPalindromeAnalyzer();
    }

}
