package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class MarinPalindromeAnalyzerTest extends PalindromeAnalyzerTest {

    @Override
    protected PalindromeAnalyzer getAnalyzerImplementation() {
        return new MarinPalindromeAnalyzer();
    }

}
