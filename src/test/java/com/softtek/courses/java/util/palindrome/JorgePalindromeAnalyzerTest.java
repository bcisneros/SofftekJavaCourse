package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class JorgePalindromeAnalyzerTest extends PalindromeAnalyzerTest {

    @Override
    protected PalindromeAnalyzer getAnalyzerImplementation() {
        return new JorgePalindromeAnalyzer();
    }

}
