package com.softtek.courses.java.util.palindrome;

import java.util.ArrayList;
import java.util.List;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Academia
 */
@RunWith(JUnitParamsRunner.class)
public abstract class PalindromeAnalyzerTest {

    private PalindromeAnalyzer analyzer;

    @Before
    public void setUp() {
        analyzer = getAnalyzerImplementation();
    }

    @Test
    @Parameters(method = "getRealPalindromes")
    public void testIsPalindromeDefaultMethodWithRealPalindromes(String phrase) {
        assertTrue(analyzer.isPalindrome(phrase));
    }

    @Test
    @Parameters(method = "getNotRealPalindromes")
    public void testIsPalindromeDefaultMethodWithNotRealPalindromes(String phrase) {
        assertFalse(analyzer.isPalindrome(phrase));
    }

    @Test
    @Parameters(method = "getRealPalindromes")
    public void testIsPalindromeOverrideMethodWithRealPalindromes(String phrase) {
        assertTrue(analyzer.isPalindrome(phrase, false));
        assertFalse(analyzer.isPalindrome(phrase, true));
    }

    @Test
    @Parameters(method = "getNotRealPalindromes")
    public void testIsPalindromeOverrideMethodWithNotRealPalindromes(String phrase) {
        assertFalse(analyzer.isPalindrome(phrase, false));
        assertFalse(analyzer.isPalindrome(phrase, true));
    }

    @Test
    public void testIsPalindromeNumber() {
        assertTrue(analyzer.isPalindrome("5005"));
        assertTrue(analyzer.isPalindrome("5005", true));
        assertTrue(analyzer.isPalindrome("5005", false));
    }

    public static List<String> getRealPalindromes() {
        List<String> realPalindromes = new ArrayList<>();
        realPalindromes.add("A man, a plan, a canal, Panama!");
        realPalindromes.add("Amor, Roma");
        realPalindromes.add("Race car");
        realPalindromes.add("Stack cats");
        realPalindromes.add("Step on no pets");
        realPalindromes.add("Taco cat");
        realPalindromes.add("Put it up");
        realPalindromes.add("Was it a car or a cat I saw?");
        realPalindromes.add("No 'x' in Nixon");
        realPalindromes.add("Anita lava la tina");

        return realPalindromes;
    }

    public static List<String> getNotRealPalindromes() {
        List<String> notRealPalindromes = new ArrayList<>();
        notRealPalindromes.add("This is not a palindrome");
        notRealPalindromes.add(null);
        notRealPalindromes.add("%$&((%$%");
        notRealPalindromes.add("");
        notRealPalindromes.add("        ");
        notRealPalindromes.add("123456789");
        return notRealPalindromes;
    }

    protected abstract PalindromeAnalyzer getAnalyzerImplementation();

}
