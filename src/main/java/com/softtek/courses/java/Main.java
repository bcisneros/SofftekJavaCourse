package com.softtek.courses.java;

import com.softtek.courses.java.util.palindrome.FritzPalindromeAnalyzer;
import com.softtek.courses.java.util.palindrome.NotValidPalindromeException;
import com.softtek.courses.java.util.palindrome.PalindromeComplexity;
import com.softtek.courses.java.util.palindrome.PalindromeComplexityCalculator;
import com.softtek.courses.java.util.printer.Printer;
import com.softtek.courses.java.util.printer.SofttekPrinter;
import com.softtek.courses.java.util.reader.Reader;
import com.softtek.courses.java.util.reader.SofttekReader;
import org.apache.log4j.Logger;

/**
 *
 * @author Academia
 */
public class Main {

    public static Printer printer = new SofttekPrinter();
    public static PalindromeComplexityCalculator calculator = new PalindromeComplexityCalculator(new FritzPalindromeAnalyzer());
    public static Reader reader = new SofttekReader();

    public static void main(String[] args) {
        printer.print("Welcome to softtek!");
        printer.print("Introduce palindrome:\n");
        String palindrome = reader.read();
        try {
            final PalindromeComplexity complexity = calculator.calculateComplexity(palindrome);
            printer.print(palindrome + " has " + complexity.getValue() + " complexity.");
            logger.debug(complexity.getValue());
        } catch (NotValidPalindromeException ex) {
            printer.print(palindrome + " is not a palindrome");
            logger.error("ERROR", ex);
        }

    }

    final static Logger logger = Logger.getLogger(Main.class);

}
