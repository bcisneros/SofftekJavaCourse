package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class DiegoBooleanStringParser implements BooleanStringParser {

    @Override
    public Boolean parseString(String value) {

        String[] trueValues = {"si", "yes", "true", "1"};
        String[] falseValues = {"no", "false", "-1", "0"};

        if (value == null) {
            return null;
        }
        for (String trueValue : trueValues) {
            if (trueValue.equals(value.toLowerCase())) {
                return true;
            }
        }
        for (String falseValue : falseValues) {
            if (falseValue.equals(value.toLowerCase())) {
                return false;
            }
        }

        return null;

    }

}
