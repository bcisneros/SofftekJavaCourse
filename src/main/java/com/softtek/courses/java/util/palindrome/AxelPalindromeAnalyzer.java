package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class AxelPalindromeAnalyzer extends AbstractPalindromeAnalyzer {

    @Override
    public boolean isPalindrome(String phrase, boolean caseSensitive) {

        if (phrase == null) {
            return false;
        }
        phrase = phrase.replace("'", "").replace(",", "").replace(" ", "").replace("!", "").replace("?", "");
        if (!caseSensitive) {
            phrase = phrase.toLowerCase();
        }
        if (phrase.isEmpty()) {
            return false;
        }

        for (int i = 0, j = phrase.length() - 1; i < phrase.length() / 2; i++, j--) {
            if (phrase.charAt(i) != phrase.charAt(j)) {
                return false;
            }
        }
        return true;
    }
}
