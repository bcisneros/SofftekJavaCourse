package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class FritzBooleanStringParser implements BooleanStringParser {

    @Override
    public Boolean parseString(String value) {
        // checking for null value
        if (value == null) {
            return null;
        }

        // check for true values
        if (value.equalsIgnoreCase("si") || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("true") || value.equals("1")) {
            return true;
        }

        // check for false values
        if (value.equalsIgnoreCase("no") || value.equalsIgnoreCase("false") || value.equals("0") || value.equals("-1")) {
            return false;
        }

        return null;
    }

}
