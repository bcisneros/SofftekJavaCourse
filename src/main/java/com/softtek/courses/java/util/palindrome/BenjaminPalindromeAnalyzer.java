package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Benjamin
 */
public class BenjaminPalindromeAnalyzer extends AbstractPalindromeAnalyzer {

    @Override
    public boolean isPalindrome(String phrase, boolean caseSensitive) {
        if (phrase == null) {
            return false;
        }
        String tempPhrase = phrase.replaceAll("[\\s\\W]", "");
        if (tempPhrase.isEmpty()) {
            return false;
        }

        String reversedPhrase = new StringBuilder(tempPhrase).reverse().toString();
        return caseSensitive ? tempPhrase.equals(reversedPhrase) : tempPhrase.equalsIgnoreCase(reversedPhrase);
    }

}
