package com.softtek.courses.java.util.reader;

/**
 *
 * @author Academia
 */
public interface Reader {

    public String read();
}
