package com.softtek.courses.java.util.printer;

/**
 *
 * @author Academia
 */
public interface Printer {

    public void print(String message);
}
