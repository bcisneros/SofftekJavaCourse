package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class GeraBooleanStringParser implements BooleanStringParser {

    @Override
    public Boolean parseString(String value) {
        System.out.println("valor es: " + value);
        if (value == null) {
            return null;
        }
        if (value.equalsIgnoreCase("no") || value.equalsIgnoreCase("false")) {
            return false;
        }
        if (value.equalsIgnoreCase("si") || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("true")) {
            return true;
        }
        try {
            Double num = Double.parseDouble(value);
            return num > 0;
        } catch (Exception e) {
            return null;
        }

    }
}
