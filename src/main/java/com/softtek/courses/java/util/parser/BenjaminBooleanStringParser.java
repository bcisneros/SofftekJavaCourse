package com.softtek.courses.java.util.parser;

import java.util.Arrays;

/**
 *
 * @author Benjamin
 */
public class BenjaminBooleanStringParser implements BooleanStringParser {

    @Override
    public Boolean parseString(String value) {
        if (value == null) {
            return null;
        }
        // Check for true values
        String[] trueValues = new String[]{"si", "yes", "true"};
        if (Arrays.asList(trueValues).contains(value.toLowerCase())) {
            return true;
        }

        String[] falseValues = new String[]{"no", "false"};
        if (Arrays.asList(falseValues).contains(value.toLowerCase())) {
            return false;
        }

        try {
            Double numericValue = Double.parseDouble(value);
            return numericValue > 0;

        } catch (Exception e) {
            return null;
        }
    }

}
