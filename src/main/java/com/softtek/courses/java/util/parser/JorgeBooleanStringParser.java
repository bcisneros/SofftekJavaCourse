package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class JorgeBooleanStringParser implements BooleanStringParser {

    @Override
    public Boolean parseString(String value) {
        if (value == null) {
            return null;
        }
        if (value.equalsIgnoreCase("no") || value.equalsIgnoreCase("false")) {
            return false;
        }
        if (value.equalsIgnoreCase("si") || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("true")) {
            return true;
        }
        try {
            Double x = Double.parseDouble(value);
            return x > 0;
        } catch (Exception e) {
            return null;
        }

    }

}
