package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public abstract class AbstractPalindromeAnalyzer implements PalindromeAnalyzer {

    @Override
    public boolean isPalindrome(String phrase) {
        return isPalindrome(phrase, false);
    }

}
