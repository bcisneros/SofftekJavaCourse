package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class NotValidPalindromeException extends Exception {

    public NotValidPalindromeException(String message) {
        super(message);
    }
}
