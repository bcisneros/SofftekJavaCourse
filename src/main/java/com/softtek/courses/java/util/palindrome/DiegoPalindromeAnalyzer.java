package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class DiegoPalindromeAnalyzer extends AbstractPalindromeAnalyzer {

    @Override
    public boolean isPalindrome(String phrase, boolean caseSensitive) {
        if (phrase == null) {
            return false;
        }

        if (!caseSensitive) {
            return palindromeSolve(phrase.toLowerCase());
        } else {
            return palindromeSolve(phrase);
        }
    }

    private boolean palindromeSolve(String phrase) {
        boolean valor = true;
        int i, ind;
        if (!phrase.contentEquals("[0-9]+")) {
            phrase = phrase.replaceAll("[\\W]", "");
        }
        phrase = phrase.replaceAll("\\s", "");

        ind = phrase.length();
        if (ind > 0) {
            for (i = 0; i < (phrase.length()); i++) {
                if (phrase.substring(i, i + 1).equals(phrase.substring(ind - 1, ind)) == false) {
                    valor = false;
                    break;
                }
                ind--;
            }
        } else {
            valor = false;
        }
        return valor;
    }

}
