package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class PalindromeComplexityCalculator {

    private final PalindromeAnalyzer analyzer;

    public PalindromeComplexityCalculator(PalindromeAnalyzer analyzer) {
        this.analyzer = analyzer;
    }

    public PalindromeComplexity calculateComplexity(String palindrome) throws NotValidPalindromeException {

        if (!analyzer.isPalindrome(palindrome)) {
            throw new NotValidPalindromeException(palindrome + " is mot a palindrome");
        }
        if (palindrome.length() <= 10) {
            return PalindromeComplexity.LOW;
        } else if (palindrome.length() <= 20) {
            return PalindromeComplexity.MEDIUM;
        } else {
            return PalindromeComplexity.HIGH;
        }
    }

}
