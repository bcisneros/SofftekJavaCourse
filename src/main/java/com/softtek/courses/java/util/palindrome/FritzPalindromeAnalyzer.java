package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class FritzPalindromeAnalyzer extends AbstractPalindromeAnalyzer {

    @Override
    public boolean isPalindrome(String phrase, boolean caseSensitive) {
        if (phrase == null) {
            return false;
        }

        //removing spaces and special characters
        String phr = phrase.replaceAll("[\\s\\W]", "");

        if (caseSensitive) {
            return validatePalindrome(phr);
        } else {
            return validatePalindrome(phr.toLowerCase());
        }
    }

    private boolean validatePalindrome(String palindrome) {
        if (palindrome.equals("")) {
            return false;
        }
        for (int i = 0; i < ((palindrome.length() / 2) + 1); i++) {
            return palindrome.charAt(i) == palindrome.charAt(palindrome.length() - i - 1);
        }
        return false;
    }

}
