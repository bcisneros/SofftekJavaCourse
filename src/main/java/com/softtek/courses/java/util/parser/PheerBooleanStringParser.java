package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class PheerBooleanStringParser implements BooleanStringParser {

    @Override
    public Boolean parseString(String value) {
        Boolean returnedValue = null;
        if (value == null || value.isEmpty()) {
            return returnedValue;
        }

        if ("si".equals(value.toLowerCase()) || "yes".equals(value.toLowerCase()) || "true".equals(value.toLowerCase())) {
            return true;
        }

        if ("no".equals(value.toLowerCase()) || "false".equals(value.toLowerCase())) {
            return false;
        }

        try {
            Double num = Double.parseDouble(value);
            return num > 0;

        } catch (Exception e) {
            return null;
        } //*/       //return false
    }

}
