package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public enum PalindromeComplexity {

    LOW("Low"),
    MEDIUM("Medium"),
    HIGH("High");

    private final String value;

    PalindromeComplexity(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
