package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class AxelBooleanStringParser implements BooleanStringParser {

    @Override
    public Boolean parseString(String value) {
        if (value == null) {
            return null;
        }
        if (value.equals("si") || value.equals("Si") || value.equals("sI") || value.equals("SI") || value.equals("yes") || value.equals("YES") || value.equals("YEs") || value.equals("Yes") || value.equals("true") || value.equals("TRUE")) {
            return true;
        }

        if (value.equals("no") || value.equals("No") || value.equals("NO") || value == "nO" || value == "false" || value == "FALSE") {
            return false;
        }
        try {
            if (Integer.parseInt(value) >= 1) {
                return true;
            }
            if (Integer.parseInt(value) < 1) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return null;
        }
    }
}
