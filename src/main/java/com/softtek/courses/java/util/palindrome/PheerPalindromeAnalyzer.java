package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class PheerPalindromeAnalyzer extends AbstractPalindromeAnalyzer {

    @Override
    public boolean isPalindrome(String phrase, boolean caseSensitive) {
        if (phrase == null) {
            return false;
        }

        if (!caseSensitive) {
            phrase = phrase.toLowerCase();
        }

        if (phrase.contains("[0-9]+")) {
            phrase = phrase.replaceAll("[\\s]", "");

        } else {
            //    phrase = phrase.replaceAll(" ","");    
            //     phrase = phrase.replace("\\\\P{L}","");
            phrase = phrase.replaceAll("[\\W]", "");
        }
        if (phrase.equals("")) {
            return false;
        }

        String reverse = "";
        int l = phrase.length();
        for (int i = l - 1; i >= 0; i--) {
            reverse = reverse + phrase.charAt(i);
        }

        if (phrase.equals(reverse)) {
            return true;
        } else {
            return false;
        }

    }
}
