package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class MarinPalindromeAnalyzer extends AbstractPalindromeAnalyzer {
//replaceAll("[\\s\\W]","")

    @Override
    public boolean isPalindrome(String phrase, boolean caseSensitive) {

        if (phrase == null) {
            return false;
        }
        String newPhrase = phrase.replaceAll("[\\s\\W]", "");
        //newPhrase=phrase;
        if (newPhrase.equals("")) {
            return false;
        }
        StringBuilder builder = new StringBuilder(newPhrase);
        String back = builder.reverse().toString();
        if (caseSensitive) {
            return newPhrase.equals(back) ? true : false;
        } else {
            return newPhrase.toLowerCase().equals(back.toLowerCase()) ? true : false;
        }
    }

}
