package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public interface PalindromeAnalyzer {

    public boolean isPalindrome(String phrase, boolean caseSensitive);

    /**
     * Case sensitive is false by default
     *
     * @param phrase
     * @return
     */
    public boolean isPalindrome(String phrase);
}
