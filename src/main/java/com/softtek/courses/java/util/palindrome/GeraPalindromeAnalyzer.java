package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class GeraPalindromeAnalyzer extends AbstractPalindromeAnalyzer {

    @Override
    public boolean isPalindrome(String phrase, boolean caseSensitive) {
        if (phrase == null) {
            return false;
        }
        if (!caseSensitive) {
            phrase = phrase.toLowerCase();
        }
        phrase = phrase.replaceAll("[\\W\\s]", "");
        if (phrase.equals("")) {
            return false;
        }
        return checkPalindrome(phrase);
    }

    public boolean checkPalindrome(String phrase) {
        int con = 0;
        int ultima = phrase.length() - 1;
        for (int i = 0; i < phrase.length(); i++) {
            if (phrase.charAt(i) == phrase.charAt(ultima)) {
                con++;
                ultima--;
            }
        }
        return con == phrase.length();
    }
}
