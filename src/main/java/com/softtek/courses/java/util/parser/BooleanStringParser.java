package com.softtek.courses.java.util.parser;

/**
 *
 * @author Benjamin
 */
public interface BooleanStringParser {

    Boolean parseString(String value);
}
