package com.softtek.courses.java.util.reader;

import java.util.Scanner;

/**
 *
 * @author Academia
 */
public class SofttekReader implements Reader {

    @Override
    public String read() {
        return new Scanner(System.in).nextLine();
    }

}
