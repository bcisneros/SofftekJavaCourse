package com.softtek.courses.java.util.parser;

/**
 *
 * @author Academia
 */
public class MarinBooleanStringParser implements BooleanStringParser {

    @Override
    public Boolean parseString(String value) {
        Boolean regresar = true;
        if (value == null) {
            return null;
        }
        value = value.toLowerCase();
        regresar = value.equals("si") || value.equals("yes") || value.equals("true") ? true : false;
        if (regresar) {
            return regresar;
        }
        regresar = value.equals("no") || value.equals("false") ? false : true;
        if (!regresar) {
            return regresar;
        }
        try {
            double a = Double.parseDouble(value);
            return a > 0;
        } catch (Exception e) {
            return null;
        }

    }

}
