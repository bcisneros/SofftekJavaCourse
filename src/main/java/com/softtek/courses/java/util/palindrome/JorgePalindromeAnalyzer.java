package com.softtek.courses.java.util.palindrome;

/**
 *
 * @author Academia
 */
public class JorgePalindromeAnalyzer extends AbstractPalindromeAnalyzer {

    @Override
    public boolean isPalindrome(String phrase, boolean caseSensitive) {
        if (phrase == null) {
            return false;
        }
        phrase = phrase.replaceAll("[\\W\\s]", "");
        if ("".equals(phrase)) {
            return false;
        }

        if (!caseSensitive) {
            phrase = phrase.toLowerCase();
        }
        String reverse = "";

        for (int i = phrase.length() - 1; i >= 0; i--) {
            reverse = reverse + phrase.charAt(i);
        }
        return phrase.equals(reverse);
        //return phrase.equals(new StringBuilder(phrase).reverse().toString());
    }

}
